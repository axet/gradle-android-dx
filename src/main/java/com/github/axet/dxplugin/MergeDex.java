package com.github.axet.dxplugin;

import org.gradle.api.tasks.Copy;
import org.gradle.api.tasks.TaskAction;

import java.io.File;

public class MergeDex extends Copy {
    public static void dex(AssetsDex.Log log, File[] ff, File into) {
        try {
            AssetsDex.d8(log, into, ff);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @TaskAction
    protected void copy() {
        try (AssetsDex.Log log = new AssetsDex.Log(getProject())) {
            dex(log, getSource().getFiles().toArray(new File[]{}), getDestinationDir());
        }
    }
}
