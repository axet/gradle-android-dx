package com.github.axet.dxplugin;

import com.android.build.gradle.AppExtension;
import com.android.build.gradle.api.AndroidSourceDirectorySet;
import com.android.build.gradle.api.AndroidSourceSet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.gradle.api.DefaultTask;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.artifacts.ModuleVersionIdentifier;
import org.gradle.api.artifacts.ResolvedDependency;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.util.PatternSet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

public class DxPlugin implements Plugin<Project> {

    public static String JSON = "gradle-android-dx.json";

    public Object toMap(Project project, File assets, Set<ResolvedDependency> dd) {
        AssetsDex.Log log = new AssetsDex.Log(project);
        ArrayList<Object> list = new ArrayList<>();
        for (ResolvedDependency d : dd) {
            LinkedHashMap<String, Object> o = new LinkedHashMap<>();
            ModuleVersionIdentifier m = d.getModule().getId();
            String id = m.getName();
            String group = m.getGroup();
            String v = m.getVersion();
            File file = d.getModuleArtifacts().iterator().next().getFile();
            log.log("processing: " + group + ":" + id + ":" + v + " " + file);
            AssetsDex.Output out = AssetsDex.dex(log, file, assets);
            Path rel = assets.toPath().relativize(out.file.toPath());
            o.put("group", group);
            o.put("id", id);
            o.put("v", v);
            o.put("asset", rel.toString());
            if (out.opts != null && out.opts.length > 0)
                o.put("opts", out.opts);
            if (out.file.exists()) { // guava has empty dependency file
                o.put("file", file.getName());
                o.put("size", out.file.length());
                try {
                    try (InputStream is = Files.newInputStream(out.file.toPath())) {
                        String md5 = DigestUtils.md5Hex(is);
                        o.put("md5", md5);
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            } else {
                log.log("ERROR: ignoring empty dependency: " + out.file);
                o.put("empty", true);
            }
            ArrayList<Object> deps = (ArrayList<Object>) toMap(project, assets, d.getChildren());
            if (deps.size() > 0)
                o.put("deps", deps);
            list.add(o);
        }
        log.close();
        return list;
    }

    public File getAssetsPath(Project project) {
        AppExtension ss = project.getExtensions().findByType(AppExtension.class);
        AndroidSourceSet ass = ss.getSourceSets().getByName(SourceSet.MAIN_SOURCE_SET_NAME);
        AndroidSourceDirectorySet asd = ass.getAssets();
        Set<File> ff = asd.getSrcDirs();
        return ff.iterator().next();
    }

    public File jsonFile(Project project) {
        File assets = getAssetsPath(project);
        return new File(assets, JSON);
    }

    public void apply(Project project) {
        project.getTasks().getByName("clean").doFirst((task) -> {
            Project a = task.getProject();
            if (a.getConfigurations().getByName("assets").isEmpty())
                return;
            a.delete(a.fileTree(getAssetsPath(a)).matching(new PatternSet().include("**/*.dex").include("**/" + JSON)));
        });

        project.getConfigurations().create("assets");

        Task processAssetsDexs = project.getTasks().create("processAssetsDexs", DefaultTask.class, (task) -> {
            task.doLast((last) -> {
                Project a = task.getProject();
                Object o = toMap(a, getAssetsPath(a), a.getConfigurations().getByName("assets").getResolvedConfiguration().getFirstLevelModuleDependencies());
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                String str = gson.toJson(o);
                File json = jsonFile(a);
                try {
                    FileUtils.writeStringToFile(json, str, Charset.defaultCharset());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        });

        project.afterEvaluate((task) -> {
            Project a = task.getProject();
            a.getTasks().getByName("preBuild").dependsOn(processAssetsDexs);
        });
    }

}
