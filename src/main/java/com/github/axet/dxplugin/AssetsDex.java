package com.github.axet.dxplugin;

import com.android.tools.r8.CompilationFailedException;
import com.android.tools.r8.D8;
import com.android.tools.r8.D8Command;
import com.android.tools.r8.Diagnostic;
import com.android.tools.r8.DiagnosticsHandler;
import com.android.tools.r8.origin.Origin;

import org.gradle.api.Project;
import org.gradle.api.tasks.Copy;
import org.gradle.api.tasks.TaskAction;
import org.gradle.internal.jvm.Jvm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class AssetsDex extends Copy {
    public static Pattern PATTERN = Pattern.compile("\\(--min-api (\\d+)\\)");

    public static class CommandLineOrigin extends Origin {
        public static final CommandLineOrigin INSTANCE = new CommandLineOrigin();

        private CommandLineOrigin() {
            super(root());
        }

        @Override
        public String part() {
            return "Command line";
        }
    }

    public static class Log implements DiagnosticsHandler, AutoCloseable {
        public static String LOG = "gradle-android-dx.log";

        File dir;
        File log;
        PrintWriter writer;

        public Log(Project project) {
            dir = project.getBuildDir();
            log = new File(dir, LOG);
        }

        public void log(String... ss) {
            String msg = new Date() + " " + String.join(" ", ss);
            try {
                if (!dir.exists() && !dir.mkdirs() && !dir.exists())
                    throw new RuntimeException("unable to create: " + dir);
                writer = new PrintWriter(new FileWriter(log, true));
                writer.println(msg);
                writer.close();
            } catch (Exception ignore) {
                System.err.println(msg);
            }
            writer = null;
        }

        @Override
        public void error(Diagnostic error) {
            log("ERROR: " + error.getDiagnosticMessage());
        }

        @Override
        public void warning(Diagnostic warning) {
            log("WARNING: " + warning.getDiagnosticMessage());
        }

        @Override
        public void info(Diagnostic info) {
            log("INFO: " + info.getDiagnosticMessage());
        }

        public void close() {
        }
    }

    public static class Output {
        public File file;
        public String[] opts;

        public Output(File f, String[] o) {
            file = f;
            opts = o;
        }
    }

    public static String[] d8_1_5_69(Log log, File out, File... jar) { // add 'aar' support for d8 1.5.69 (gradle 3.5.4)
        ArrayList<File> tmps = new ArrayList<>();
        for (int i = 0; i < jar.length; i++) {
            File f = jar[i];
            String s = f.getAbsolutePath().toLowerCase(Locale.ROOT);
            if (s.endsWith(".aar")) {
                try {
                    ZipFile zip = new ZipFile(f);
                    File tmp = File.createTempFile(f.getName(), ".jar");
                    for (Enumeration ee = zip.entries(); ee.hasMoreElements(); ) {
                        ZipEntry entry = (ZipEntry) ee.nextElement();
                        if (entry.getName().toLowerCase(Locale.ROOT).endsWith("classes.jar")) {
                            InputStream is = zip.getInputStream(entry);
                            FileOutputStream os = new FileOutputStream(tmp);
                            byte[] buf = new byte[4096];
                            int len;
                            while ((len = is.read(buf)) > 0)
                                os.write(buf, 0, len);
                            os.close();
                            is.close();
                        }
                    }
                    log.log("aar copy to jar: " + f + " -> " + tmp);
                    jar[i] = tmp;
                    tmps.add(tmp);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
        String[] oo = _d8(log, out, jar);
        for (File t : tmps) {
            log.log("delete temp jar: " + t);
            t.delete();
        }
        return oo;
    }

    public static String[] d8(Log log, File out, File... jar) {
        return _d8(log, out, jar);
    }

    public static String[] _d8(Log log, File out, File... jar) {
        int min = 1;
        ArrayList<String> opts = new ArrayList<>();
        while (true) {
            try {
                ArrayList<String> aa = new ArrayList<>(opts);
                aa.addAll(Arrays.asList("--lib", Jvm.current().getJavaHome().toString(),
                        "--output", out.getAbsolutePath()));
                for (File f : jar)
                    aa.add(f.getAbsolutePath());
                D8Command cmd;
                try {
                    cmd = D8Command.parse(aa.toArray(new String[0]), CommandLineOrigin.INSTANCE, log).build();
                } catch (CompilationFailedException e) {
                    try {
                        Throwable c = e;
                        while ((c = c.getCause()) != null) {
                            Class AbortException = Class.forName("com.android.tools.r8.utils.AbortException");
                            if (c.getClass().isAssignableFrom(AbortException)) {
                                String msg = c.getMessage();
                                if (msg.contains("Unsupported source file type") && msg.contains(".aar")) {
                                    log.log("Compilation error .aar " + e);
                                    return d8_1_5_69(log, out, jar);
                                }
                            }
                        }
                    } catch (ClassNotFoundException e2) {
                        log.log("unable to test compilation errors " + e2);
                    }
                    throw e;
                }
                D8.run(cmd);
                return opts.toArray(new String[0]);
            } catch (CompilationFailedException e) {
                String msg = e.getCause().getMessage();
                if (msg.contains("Invoke-customs are only supported starting with Android O")) {
                    if (min >= 26)
                        throw new RuntimeException(e);
                    min = 26;
                } else if (msg.contains("Default interface methods are only supported starting with Android N")) {
                    if (min >= 24)
                        throw new RuntimeException(e);
                    min = 24;
                } else if (msg.contains("desugaring of")) { // Class or interface java.lang.Exception required for desugaring of try-with-resources is not found
                    String disg = "--no-desugaring";
                    if (opts.contains(disg))
                        throw new RuntimeException(e);
                    opts.add(disg);
                    log.log("rebuilding, enabling " + disg + " for " + out.getAbsolutePath());
                } else {
                    Matcher m = PATTERN.matcher(msg); // (--min-api 24)
                    if (m.find()) {
                        int g = Integer.parseInt(m.group(1));
                        if (min >= g)
                            throw new RuntimeException(e);
                        min = g;
                    } else {
                        throw new RuntimeException(e);
                    }
                }
                if (min > 1) {
                    String minapi = "--min-api";
                    int i = opts.indexOf(minapi);
                    if (i >= 0) {
                        opts.remove(i); // --min-api
                        opts.remove(i); // 26
                    }
                    opts.add(minapi);
                    opts.add(Integer.toString(min));
                    log.log("enabling " + minapi + " " + min);
                }
            }
        }
    }

    public static Output dex(Log log, File file, File into) {
        try {
            String name = file.getName();
            name = name.substring(0, name.lastIndexOf('.'));
            into.mkdirs();
            File out = new File(into, name + ".zip");
            File dex = new File(into, name + ".dex");
            String[] opts = d8(log, out, file);
            try {
                ZipFile zip = new ZipFile(out);
                for (Enumeration ee = zip.entries(); ee.hasMoreElements(); ) {
                    ZipEntry entry = (ZipEntry) ee.nextElement();
                    if (entry.getName().toLowerCase(Locale.ROOT).endsWith("classes.dex")) {
                        InputStream is = zip.getInputStream(entry);
                        FileOutputStream os = new FileOutputStream(dex);
                        byte[] buf = new byte[4096];
                        int len;
                        while ((len = is.read(buf)) > 0)
                            os.write(buf, 0, len);
                        os.close();
                        is.close();
                    }
                }
                out.delete();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            return new Output(dex, opts);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @TaskAction
    protected void copy() {
        final File into = getDestinationDir();
        if (!into.exists() && !into.mkdirs() && !into.exists())
            throw new RuntimeException("unable to create dir " + into);
        getSource().forEach((file) -> {
            try (Log log = new Log(getProject())) {
                dex(log, file, into);
            }
        });
    }
}
