# Gradle Android dx plugin

Plugin allows you to pack dependencies (.jar files) into android 'assets' folder (.dex files), which can be loaded later using DexClassLoader class. In partucular it helps solve 'LinearAlloc exceeded capacity' issue on old android devices.

  * https://stackoverflow.com/questions/8413898

Use 'AssetsDexLoader' to load dex:

  * https://gitlab.com/axet/android-library/blob/master/src/main/java/com/github/axet/androidlibrary/app/AssetsDexLoader.java

# Example

Plugin gradle script 100% equvalent:

```gradle
configurations {
    assets
}

clean.doFirst {
    if (!project.configurations.assets.empty) {
        delete fileTree('src/main/assets') {
            include '**/*.dex'
        }
    }
}

task processAssetsDexs() {
    doLast {
        configurations.assets.each { File file ->
            exec {
                commandLine "${android.getSdkDirectory().getAbsolutePath()}/build-tools/${android.buildToolsVersion}/dx",
                        '--dex',
                        "--output=${project.projectDir}/src/main/assets/${file.name.take(file.name.lastIndexOf('.'))}.dex",
                        "${file.getAbsolutePath()}"
            }
        }
    }
}

project.afterEvaluate {
    preBuild.dependsOn processAssetsDexs
}
```

Script above using 'dx' script from android build-tools, 'plugin' using 'com.google.android.tools:dx', it is independed from local Android SDK installation. In additional 'plugin' creates 'gradle-android-dx.json' file which helps to find dex files and load them in correct order.

# Adding .dex

Example how to add dependecies as .dex assets files:

```gradle
buildscript {
    dependencies {
        classpath 'com.github.axet:gradle-android-dx:0.3.0'
    }
}

apply plugin: 'com.github.axet.dxplugin'

dependencies {
    assets('com.madgag.spongycastle:bctls-jdk15on:1.58.0.0') { exclude module: 'junit' }
}
```

# Manual install

    # gradle publishToMavenLocal

# Additional Tasks

You can create custom tasks to extract .dex into specified directory:

```gradle
configurations {
    dexs
}

// AssetsDex task.
//
// Create corresponds .dex files for each input dependecies.
//
task ExtractDexs(type: com.github.axet.dxplugin.AssetsDex) {
    from configurations.dexs
    into 'src/main/assets'
}

// MergeDex task.
//
// Convert a set of classfiles [<file>.class | <file>.{zip,jar,apk} | <directory>]
// into a dex file, optionally embedded in a jar/zip. Output name must end with
// one of: .dex .jar .zip .apk or be a directory.
//
task PackDexs(type: com.github.axet.dxplugin.MergeDex) {
    from "lib1.jar"
    into 'src/main/assets/lib1.dex'
}

project.afterEvaluate {
    preBuild.dependsOn ExtractDexs
}

dependencies {
    dexs('com.madgag.spongycastle:bctls-jdk15on:1.58.0.0') { exclude module: 'junit' }
}
```

# Dependencies

Gradle allow to specify multiply dependencies:

```gradle
dependencies {
    assets('com.github.axet.exoplayer:exoplayer:2.7.3') { exclude group: 'com.android.support' }
    assets 'com.github.axet:wget:1.7.0'
}
```

But gradle does not allow to specify multiply versions of the same library:

```gradle
 plugins {
     id 'java' // so that there are some configurations
 }

configurations.assets {
   resolutionStrategy {
     failOnVersionConflict()
   }
}

dependencies {
    assets('com.github.axet.exoplayer:exoplayer:2.7.3') { exclude group: 'com.android.support' }
    assets('com.github.axet.exoplayer:exoplayer:2.5.4') { exclude group: 'com.android.support' }
}
```

### Compiler

You can switch compiler version used to prepare dex files using gradle dependecy managment:

```gradle
buildscript {
    dependencies {
        classpath ('com.github.axet:gradle-android-dx:0.3.1') { exclude module: 'gradle' }
        classpath ('com.android.tools.build:gradle:3.5.4') // gradle wrapper 5.4.1, compiler: d8 (1.5.69)
    }
}
```

or:

```gradle
buildscript {
    dependencies {
        classpath ('com.github.axet:gradle-android-dx:0.3.1') { exclude module: 'gradle' }
        classpath ('com.android.tools.build:gradle:4.0.2') // gradle wrapper 6.1.1, compiler: d8 (2.0.99)
    }
}
```

To get a d8 compiler version from gradle jar, you have to call java on builder jar (gradle depenency).

```bash
java -cp $HOME/.gradle/caches/*/*/com.android.tools.build/builder/3.5.4/*/builder-3.5.4.jar com.android.tools.r8.D8 --version
```

### gradle-android-dx-0.3.1

java 1.8 support. gradle 3.5.4 (gradle wrapper 5.4.1)  compiler: d8 (1.5.69)

### gradle-android-dx-0.2.4

java 1.8 support. gradle 4.0.2 (gradle wrapper 6.1.1) compiler: d8 (2.0.99)

### gradle-android-dx-0.1.3

java 1.7 support. gradle 4.0.2 (gradle wrapper 6.1.1) compiler: dx (1.7)
